name := "mapredrns"

version := "1.0"

javacOptions ++= Seq("-source", "1.7", "-target", "1.7")

javacOptions in doc := Seq("-source", "1.7")

// Do not append Scala versions to the generated artifacts
crossPaths := false

// This forbids including Scala related libraries into the dependency
autoScalaLibrary := false

// library dependencies. (orginization name) % (project name) % (version)
libraryDependencies ++= Seq(
  "org.apache.commons" % "commons-math3" % "3.1.1",
  "junit" % "junit" % "4.10",
  "com.novocode" % "junit-interface" % "0.10-M1" % "test"
)

libraryDependencies += "org.apache.hadoop" % "hadoop-mapreduce-client-core" % "2.4.0"

libraryDependencies += "org.apache.hadoop" % "hadoop-common" % "2.4.0"