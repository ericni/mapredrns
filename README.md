# Parallel Ranking and Selection Using MapReduce

This is a MapReduce implementation of the GSP procedure for solving Ranking and Selection problems. The procedure is described in detail in the working paper 

> Ni, Eric C., Dragos F. Ciocan, Susan R. Hunter, Shane G. Henderson (2015), "Efficient Ranking and Selection in Parallel Computing Environments". http://arxiv.org/abs/1506.04986

To compile, simply run the following command (which uses [SBT](http://www.scala-sbt.org/)) and a jar file will appear in the */target* folder. 

    sbt/sbt clean package

The default version is built against Hadoop 1.2.1.  Checkout branch *hadoop2* for a new version built against Hadoop 2.4.0 and the new *org.apache.hadoop.mapreduce* API.

    git fetch && git checkout hadoop2 

The javadoc documentation of this package can be found [here](http://people.orie.cornell.edu/cn254/MapRedRnS_docs/index.html).