package driver;

import static org.junit.Assert.*;

import org.junit.*;

import org.apache.commons.math3.distribution.*;

/**
 * Simple JUnit test for the Rinott.CHIPDF method.
 * @author cn254
 *
 */
public class Test_ChiSq {

	@Test
	public void test() {
		double LNGAM[] = new double[50];
		LNGAM[0] = 0.5723649429;
		LNGAM[1] = 0.0;
		for (int i = 2; i <= 25; ++i) {
			LNGAM[2*i - 2] = Math.log(i - 1.5) + LNGAM[2*i - 4];
			LNGAM[2*i - 1] = Math.log(i - 1.0) + LNGAM[2*i - 3];
		}
		int 	df[]	= { 4,		50,		20,		20,		50		};
		double 	x[]		= { .5,		1,		2,	 	3,	   	.25	};
		for (int i = 0; i < x.length; ++i) {

			ChiSquaredDistribution chisqDist = new ChiSquaredDistribution(df[i]);
			double own    = chisqDist.density(x[i]);
			double approx = Rinott.CHIPDF(df[i], x[i], LNGAM);
			assertEquals( own, approx, 0.001);
		}
	}
}
