/*
 * Eric Ni, Nov 2014
 * School of Operations Research and Information Engineering
 * cn254@cornell.edu
 */

package driver;

import java.io.IOException;
import java.util.*;

import rngstream.*;
import problems.*;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.mapred.lib.*;
//import org.apache.hadoop.mapreduce.*;

import driver.Rinott;

/**
 * Parallel ranking and selection with MapReduce.
 * 
 * Implements procedure GSP by Ni et, al. (2015)
 *  
 * @author cn254
 */
public class prands {
	enum Counters {
		SURVIVING_SYS, NUM_SYS, TOTAL_S2T, NUM_RINOTT, RINOTT_SIZE, BEST_ID, REP_COUNT,
		P0_TIME, P1_TIME, P2_TIME, SCREEN_TIME, MAP_TIME, RED_TIME
	}

	/**
	 * Initial simulation for batches of size n1, to obatain variance estimates.
	 * <br>
	 * Input: [i] - System ID
	 * <br>
	 * Output: i: {Xbar S^2 stream "$S0"}
	 * @author cn254
	 */
	public static class Stage1InitMapper extends MapReduceBase 
	implements Mapper<LongWritable, Text, IntWritable, Text> {
		/**
		 * Number of replications in Stage 1
		 */
		int n1;
		/**
		 * Initial seed for random number generator
		 */
		int globalseed;
		/**
		 * Used to tokenize input
		 */
		private List<String> tokenList = new ArrayList<String>();
		/**
		 * Parameter RB for the TpMax test problem
		 */
		private String param = null;
		/**
		 * Parameter for randomizing completion time
		 */
		private String cov = null;

		/**
		 * Reads parameters from the JobConf object
		 */
		public void configure(JobConf conf) {
			n1 = conf.getInt("n1", 50);
			globalseed =  conf.getInt("seed", 14853);
			param = conf.get("param");
			cov = conf.get("cov");
		}
		public void map(LongWritable key, Text value,
				OutputCollector<IntWritable, Text> output, Reporter reporter)
						throws IOException {
			long _tt = System.currentTimeMillis();
			tokenList.clear();
			String line = value.toString();
			StringTokenizer tokenizer = new StringTokenizer(line);
			while (tokenizer.hasMoreTokens())
				tokenList.add(tokenizer.nextToken());
			int sysid = Integer.parseInt(tokenList.get(0));
			long [] seed = null;			
			if (tokenList.size() >= 2) 		
				seed = RngStream.StrToSeed(tokenList.get(1));
			else {
				seed = new long[6];
				for(int i = 0; i < 6; ++i)
					seed[i] = (long) sysid+globalseed;
			}
			RngStream rStream = new RngStream();
			rStream.setSeed(seed);
			SOProb prob = new TpMax(param, cov);
			long _t = System.nanoTime();
			prob.runSystem(sysid, n1, rStream);
			_t = System.nanoTime() - _t;
			reporter.incrCounter(Counters.P0_TIME, _t);
			SOAnswer ans = prob.getAns();
			double fn = ans.getFn();
			double s2 = ans.getFnVar();
			double t = (double) ans.getSimtime();
			double [] state = rStream.getState();

			for(int i = 0; i < 6; ++i) 
				seed[i] = (long) state[i];

			//			double[] ans = run_Simulation( sysid, n1 );	
			String outputStr = Double.toString(fn) + " " + Double.toString(s2) + " " 
					+ Double.toString(t) + " " + RngStream.SeedToStr(seed)
					+ " $S0";

			output.collect(new IntWritable( sysid ), new Text( outputStr ));
			_tt = System.currentTimeMillis() - _tt;
			reporter.incrCounter(Counters.MAP_TIME, _tt);
		}
	}

	/**
	 * Initial reducer that adds S across all systems using Counter.
	 * <br>
	 * Input: i: {Xbar S^2 stream "$S0"}
	 * <br>
	 * Output: [i Xbar S^2 stream "$S0"]
	 * @author cn254
	 */
	public static class Stage1InitReducer extends MapReduceBase 
	implements Reducer<IntWritable, Text, IntWritable, Text> {
		//		private MultipleOutputs<IntWritable, Text> out;
		/**
		 * Used to tokenize input
		 */
		private List<String> tokenList = new ArrayList<String>();
		//		@Override
		//		public void setup(OutputCollector<IntWritable, Text> output, Reporter reporter) {
		//			out = new MultipleOutputs<IntWritable, Text>(context);
		//		}
		public void reduce(IntWritable key, Iterator<Text> values,
				OutputCollector<IntWritable, Text> output, Reporter reporter)
						throws IOException {
			long _tt = System.currentTimeMillis();
			tokenList.clear();
			reporter.incrCounter(Counters.NUM_SYS, 1);
			//			reporter.getCounter(MapRedRnS.Counters.NUM_SYS).increment(1);
			String ans = null;
			while(values.hasNext()) {
				ans = values.next().toString();
			}
			StringTokenizer tokenizer = new StringTokenizer(ans);
			while (tokenizer.hasMoreTokens())
				tokenList.add(tokenizer.nextToken());
			double S2 = Double.parseDouble(tokenList.get(1));

			String answer = tokenList.get(0) + " " + tokenList.get(1)
					+ " " + tokenList.get(2) + " " + tokenList.get(3) + " $Stage0";

			reporter.incrCounter(Counters.TOTAL_S2T, (long)(Math.sqrt(S2)*10000.));
			//			reporter.getCounter(MapRedRnS.Counters.TOTAL_S2T).increment((long)(Math.sqrt(S2/t)*10000.));
			//			calculate beta and add to the counter
			//			double beta = 100;
			//			context.getCounter(MapRedRnS.Counters.TOTAL_BETA).increment((long)beta * 10000);
			output.collect(key, new Text(answer));

			//			out.write("altern", key, new Text(tokenList.get(2)), outputPath + "/Stage0Seed");

			//			output.collect(key, new Text(tokenList.get(2)));
			_tt = System.currentTimeMillis() - _tt;
			reporter.incrCounter(Counters.RED_TIME, _tt);
		}


		//		@Override
		//		public void cleanup(OutputCollector<IntWritable, Text> output, Reporter reporter) throws IOException {
		//			out.close();
		//		}
	}

	/**
	 * Calculates batch size based on Stage 1 output.
	 * <br>
	 * Input: [i Xbar S^2 stream "$S0"]
	 * <br>
	 * Output: Group(i): {i Xbar n1 batchSize S^2 stream "$Sim"}
	 * @author cn254
	 */
	public static class Stage1Mapper extends MapReduceBase 
	implements Mapper<LongWritable, Text, IntWritable, Text> {

		/**
		 * Used to tokenize input
		 */
		private List<String> tokenList = new ArrayList<String>();

		/**
		 * Average S2 calculated by the driver
		 */
		double avgS;
		/**
		 * Total number of systems
		 */
		long nSys;
		/**
		 * Average batch size parameter beta
		 */
		int avgBatchSize;
		/**
		 * Number of screening groups
		 */
		int nGroups = 2;
		/**
		 * Stage 1 sample size
		 */
		int n1;
		/**
		 * Reads parameters from the JobConf object
		 */
		public void configure(JobConf conf) {
			n1 = conf.getInt("n1", 50);
			nGroups = conf.getInt("nGroups", 10);
			avgS = Double.parseDouble(conf.get("avgS", "1000"));
			nSys = conf.getLong("TotalSys", 1000000);
			avgBatchSize = conf.getInt("batchSize", 200);
		}
		public void map(LongWritable key, Text value,
				OutputCollector<IntWritable, Text> output, Reporter reporter)
						throws IOException {
			long _tt = System.currentTimeMillis();
			tokenList.clear();
			String line = value.toString();
			StringTokenizer tokenizer = new StringTokenizer(new String(line));
			while (tokenizer.hasMoreTokens())
				tokenList.add(tokenizer.nextToken());

			int sysid = Integer.parseInt(tokenList.get(0));
			//			if(line.endsWith("$Stage0")) {
			double fn  = Double.parseDouble(tokenList.get(1));
			double S2 = Double.parseDouble(tokenList.get(2));
			String seedStr = tokenList.get(4);
			//calculate batch size
			int batchsize = (int) Math.ceil(Math.sqrt(S2) / avgS * avgBatchSize);
			Text answer = new Text( sysid + " " + Double.toString(fn) + " " 
					+ n1 + " " + batchsize + " "
					+ Double.toString(S2) + " " + seedStr + " $Iter");
			output.collect( new IntWritable( SOProb.get_group(sysid, nGroups) ), answer);
			_tt = System.currentTimeMillis() - _tt;
			reporter.incrCounter(Counters.MAP_TIME, _tt);
		}
	}

	/**
	 * Runs simulation for one iteration.
	 * <br>
	 * Input: [i Xbar n batchSize S^2 stream "$Sim"]
	 * <br>
	 * Output: Group(i): {i Xbar n1 batchSize S^2 stream "$Sim"} with a larger sample
	 * @author cn254
	 */
	public static class Stage2Mapper extends MapReduceBase 
	implements Mapper<LongWritable, Text, IntWritable, Text> {

		/**
		 * Used to tokenize input
		 */
		private List<String> tokenList = new ArrayList<String>();

		/**
		 * Parameter RB for the TpMax test problem
		 */
		private String param = null;
		/**
		 * Parameter for randomizing completion time
		 */
		private String cov = null;

		/**
		 * Number of screening groups
		 */
		int nGroups = 2;
		/**
		 * Reads parameters from the JobConf object
		 */
		public void configure(JobConf conf) {
			nGroups = conf.getInt("nGroups", 10);
			param = conf.get("param");
			cov = conf.get("cov");
		}
		public void map(LongWritable key, Text value,
				OutputCollector<IntWritable, Text> output, Reporter reporter)
						throws IOException {
			long _tt = System.currentTimeMillis();
			tokenList.clear();
			String line = value.toString();
			StringTokenizer tokenizer = new StringTokenizer(line);
			while (tokenizer.hasMoreTokens())
				tokenList.add(tokenizer.nextToken());

			int sysid = Integer.parseInt(tokenList.get(0));
			int grp = SOProb.get_group(sysid, nGroups);
			//			if (line.endsWith("$Stage0") ) {
			//				// Stage 0 file: id S2 t $Stage0
			//				double S2 = Double.parseDouble(tokenList.get(1));
			//				double t = Double.parseDouble(tokenList.get(2));
			//				Text id_S2_and_t = new Text( Integer.toString(sysid) + " "
			//						+ Double.toString(S2) + " " + Double.toString(t) + " $Stage0" );
			//				output.collect(new IntWritable(grp), id_S2_and_t);
			//			} else 
			if (line.endsWith("$Iter")) {
				// iter i-1 file: sysid Xbar n size seedStr $Iter
				double Xbar = Double.parseDouble(tokenList.get(1));
				int n = Integer.parseInt(tokenList.get(2));
				int size = Integer.parseInt(tokenList.get(3));	
				double S2 = Double.parseDouble(tokenList.get(4));
				long [] seed = RngStream.StrToSeed(tokenList.get(5));

				//get answer and update Xbar, n
				RngStream.setPackageSeed(seed);
				RngStream rStream = new RngStream();
				SOProb prob = new TpMax(param,cov);
				long _t = System.nanoTime();
				prob.runSystem(sysid, size, rStream);
				_t = System.nanoTime() - _t;
				reporter.incrCounter(Counters.P1_TIME, _t);
				reporter.incrCounter(Counters.REP_COUNT, size);
				SOAnswer ans = prob.getAns();
				double Xbarnew = ( ans.getFn() * size + Xbar * n ) / (size + n);
				int nnew = size + n;
				double [] state = rStream.getState();
				for(int i = 0; i < 6; ++i) 
					seed[i] = (long) state[i];

				String result = Integer.toString(sysid) + " " + 
						Double.toString(Xbarnew) + " " + 
						Integer.toString(nnew) + " " + 
						Integer.toString(size) + " " + 
						Double.toString(S2) + " " +
						RngStream.SeedToStr(seed)
						+ " $Iter";
				output.collect(new IntWritable(grp), new Text(result));
			}
			_tt = System.currentTimeMillis() - _tt;
			reporter.incrCounter(Counters.MAP_TIME, _tt);
		}
	}


	/**
	 * Screens within a group.
	 * <br>
	 * Input: Group: {i Xbar n batchSize S^2 stream "$Sim"}
	 * <br>
	 * Output 1: [i Xbar n batchSize S^2 stream "$Sim"] for each surviving system i
	 * <br>
	 * Output 2: [i Xbar n batchSize S^2 "$Best"] for the best system i*
	 * @author cn254
	 */
	public static class GroupScreenReducer extends MapReduceBase 
	implements Reducer<IntWritable, Text, IntWritable, Text> {

		/**
		 * Used to tokenize input
		 */
		private List<String> tokenList = new ArrayList<String>();
		/**
		 * ID of the best system 
		 */
		private int bestid = -1;
		/**
		 * SOAnswer container for the best system
		 */
		private SOAnswer bestAns = null;
		/**
		 * Maps each system ID to SOAnswer object
		 */
		private Map<Integer, SOAnswer> answerMap = new HashMap<Integer, SOAnswer>();
		/**
		 * Parameter rbar, maximum number of batches in Stage 2
		 */
		private int rbar = 200;
		/**
		 * Parameter eta
		 */
		private double eta = 0.0;
		/**
		 * Stage 1 sample size
		 */
		private int n1=50;
		/**
		 * Reads parameters from the JobConf object
		 */
		public void configure(JobConf conf) {
			rbar = conf.getInt("maxBatch", 200);
			n1 = conf.getInt("n1", 50);
			eta = Double.parseDouble(conf.get("eta"));
		}
		//		private MultipleOutputs<IntWritable, Text> out;
		//
		//		@Override
		//		public void setup(OutputCollector<IntWritable, Text> output, Reporter reporter) {
		//			out = new MultipleOutputs<IntWritable, Text>(context);
		//		}

		private void screen() {
			List<Integer> IDs = new ArrayList<Integer>(answerMap.keySet());
			int nSys = IDs.size();
			for (int i = 0; i < nSys; ++i) {
				SOAnswer ans_i = answerMap.get(IDs.get(i));
				if (ans_i.getElim() != 0) continue;
				if (bestAns == null || ans_i.getFn() > bestAns.getFn()) {
					bestid = IDs.get(i);
					bestAns = ans_i;
				}
				double Xbar_i = ans_i.getFn();
				int ni = ans_i.getSize();
				double S2i = ans_i.getFnVar();
				int bi = ans_i.getBatchSize();
				for (int j = i + 1; j < nSys; ++j) {
					SOAnswer ans_j = answerMap.get(IDs.get(j));
					if (ans_j.getElim() != 0) continue;
					double Xbar_j = ans_j.getFn();
					int nj = ans_j.getSize();
					double S2j = ans_j.getFnVar();
					int bj = ans_j.getBatchSize();
					int maxi = n1 + rbar * bi;
					int maxj = n1 + rbar * bj;
					double tij = 1. / ( (S2i / maxi) + (S2j / maxj));
					double aij = Math.sqrt( (double) (tij * (n1-1)) ) * eta;
					double tau = 1.0/( (S2i/ni)+(S2j/nj) );
					double Y = tau*(Xbar_i-Xbar_j);
					// calculate aij based on maxBatch

					if (Y < -aij ) {
						// system i eliminated by system j
						ans_i.setElim(1);
					} else if (Y > aij) {
						// system j eliminated by system i
						ans_j.setElim(1);
					}
				}
			}
		}

		public void reduce(IntWritable key, Iterator<Text> values,
				OutputCollector<IntWritable, Text> output, Reporter reporter)
						throws IOException {
			long _tt = System.currentTimeMillis();
			tokenList.clear();
			answerMap.clear();
			bestAns = null;
			//			rmax = context.getConfiguration().getInt("maxBatch", 200);

			while(values.hasNext()) {
				String line = values.next().toString();
				tokenList.clear();
				StringTokenizer tokenizer = new StringTokenizer(line);
				while (tokenizer.hasMoreTokens())
					tokenList.add(tokenizer.nextToken());

				int sysid = Integer.parseInt(tokenList.get(0));
				SOAnswer ans = answerMap.get(sysid);
				if(ans == null) {
					ans = new SOAnswer();
					answerMap.put(sysid, ans);
					ans.setElim(1);
				}
				//				if (line.endsWith("$Stage0") ) {
				//					// Stage 0 file: id S2 t $Stage0
				//					double S2 = Double.parseDouble(tokenList.get(1));
				//					// Add S2 to list
				//					ans.setFnVar(S2);					
				//				} else 
				if (line.endsWith("$Iter")) {
					// iter i-1 file: sysid Xbar n size seedStr $Iter
					double Xbar = Double.parseDouble(tokenList.get(1));
					int n = Integer.parseInt(tokenList.get(2));
					int batchSize = Integer.parseInt(tokenList.get(3));
					double S2 = Double.parseDouble(tokenList.get(4));	
					String seedStr = tokenList.get(5);
					// Add Xbar and n to list
					ans.setSize(n);
					ans.setFn(Xbar);
					ans.setSeedString(seedStr);
					ans.setFnVar(S2);
					ans.setBatchSize(batchSize);
					ans.setElim(0);
				}				
			}
			long _t = System.currentTimeMillis();
			screen();
			_t = System.currentTimeMillis() - _t;
			reporter.incrCounter(Counters.SCREEN_TIME, _t);


			// for the best sys
			double bestXbar = bestAns.getFn();
			int bestn = bestAns.getSize();
			double bestS2 = bestAns.getFnVar();
			int bestBatchSize = bestAns.getBatchSize();
			String best_line = Double.toString(bestXbar) + " "
					+ Integer.toString(bestn) + " " 
					+ Integer.toString(bestBatchSize)  + " " 
					+ Double.toString(bestS2)+" $Best";
			//			int iteration = context.getConfiguration().getInt("iteration", 1);
			output.collect(new IntWritable(bestid), new Text(best_line));
			//			out.write(new IntWritable(bestid), new Text(best_line), "best/part" + (iteration));

			// for each surviving system
			List<Integer> IDs = new ArrayList<Integer>(answerMap.keySet());
			int nSys = IDs.size();
			for (int i = 0; i < nSys; ++i) {
				int sysid = IDs.get(i);
				SOAnswer ans = answerMap.get(sysid);
				if (ans.getElim() == 0) {
					reporter.incrCounter(Counters.SURVIVING_SYS, 1);
					double Xbar = ans.getFn();
					int n = ans.getSize();
					int batchSize = ans.getBatchSize();
					double S2 = ans.getFnVar();
					String line = Double.toString(Xbar) + " " + Integer.toString(n) + " " +
							Integer.toString(batchSize) + " " + 
							Double.toString(S2) + " " + ans.getSeedString() + " $Iter";
					output.collect(new IntWritable(sysid), new Text (line));
					//				out.write(new IntWritable(sysid), new Text (line), "group/part");
				}
			}
			_tt = System.currentTimeMillis() - _tt;
			reporter.incrCounter(Counters.RED_TIME, _tt);
		}

		//		@Override
		//		public void cleanup(OutputCollector<IntWritable, Text> output, Reporter reporter) throws IOException {
		//			out.close();
		//		}
	}

	/**
	 * Share best systems between groups.
	 * <br>
	 * Input 1: [i Xbar n batchSize S^2 stream "$Sim"]
	 * <br>
	 * Output 1: Group(i): {i Xbar n batchSize S^2 stream "$Sim"}
	 * <br>
	 * Input 2: [i Xbar n batchSize S^2 $Best]
	 * <br>
	 * Output 2: Group g: {i Xbar n batchSize S^2 $best} for every group g
	 * @author cn254
	 */
	public static class ScreenBestMapper extends MapReduceBase 
	implements Mapper<LongWritable, Text, IntWritable, Text> {

		/**
		 * Used to tokenize input
		 */
		private List<String> tokenList = new ArrayList<String>();

		/**
		 * Number of screening groups
		 */
		int nGroups = 0;
		/**
		 * Reads parameters from the JobConf object
		 */
		public void configure(JobConf conf) {
			nGroups = conf.getInt("nGroups", 10);
		}
		public void map(LongWritable key, Text value,
				OutputCollector<IntWritable, Text> output, Reporter reporter)
						throws IOException {
			long _tt = System.currentTimeMillis();
			tokenList.clear();
			String line = value.toString();
			StringTokenizer tokenizer = new StringTokenizer(line);
			while (tokenizer.hasMoreTokens())
				tokenList.add(tokenizer.nextToken());

			int sysid = Integer.parseInt(tokenList.get(0));
			int grp = SOProb.get_group(sysid, nGroups);
			//			if (line.endsWith("$Stage0") ) {
			//				// Stage 0 file: id S2 t $Stage0
			//				double S2 = Double.parseDouble(tokenList.get(1));
			//				double t = Double.parseDouble(tokenList.get(2));
			//				Text id_S2_and_t = new Text( Integer.toString(sysid) + " "
			//						+ Double.toString(S2) + " " + Double.toString(t) + " $Stage0" );
			//				output.collect(new IntWritable(grp), id_S2_and_t);
			//			} else 
			if (line.endsWith("$Iter")) {
				// iter i-1 file: sysid Xbar n size seedString $Iter
				double Xbar = Double.parseDouble(tokenList.get(1));
				int n = Integer.parseInt(tokenList.get(2));
				int size = Integer.parseInt(tokenList.get(3));	
				double S2 = Double.parseDouble(tokenList.get(4));
				String seedString = tokenList.get(5);
				String result = Integer.toString(sysid) + " " + 
						Double.toString(Xbar) + " " + 
						Integer.toString(n) + " " + 
						Integer.toString(size) + " " +
						Double.toString(S2) + " " +
						seedString + " $Iter";
				output.collect(new IntWritable(grp), new Text(result));
			} else if  (line.endsWith("$Best")) {
				// best sys file: sysid Xbar n S2
				double Xbar = Double.parseDouble(tokenList.get(1));
				int n = Integer.parseInt(tokenList.get(2));
				int bestBatchSize = Integer.parseInt(tokenList.get(3));
				double S2 = Double.parseDouble(tokenList.get(4));
				String result = Integer.toString(sysid) + " " + 
						Double.toString(Xbar) + " " + 
						Integer.toString(n) + " " +						
						Integer.toString(bestBatchSize) + " " + 
						Double.toString(S2) + " $Best";	
				for (int i=0; i<nGroups; ++i) {
					output.collect(new IntWritable(i), new Text(result));
				}
			}
			_tt = System.currentTimeMillis() - _tt;
			reporter.incrCounter(Counters.MAP_TIME, _tt);
		}
	}

	/**
	 * Screen against best systems from other groups.
	 * <br>
	 * Input 1: Group g: {i Xbar n batchSize S^2 stream "$Sim"} for all systems in group g
	 * <br>
	 * Input 2: Group g': {i Xbar n batchSize S^2 stream "$Best"} from every other group g'
	 * <br>
	 * Output:  [i Xbar n batchSize S^2 stream "$Sim"] for each surviving system i
	 * 
	 */
	public static class ScreenBestReducer extends MapReduceBase 
	implements Reducer<IntWritable, Text, IntWritable, Text> {
		/**
		 * Used to tokenize input
		 */
		private List<String> tokenList = new ArrayList<String>();

		/**
		 * Parameter rbar, maximum number of batches in Stage 2
		 */
		private int rbar = 200;
		/**
		 * Parameter eta
		 */
		private double eta = 0.0;
		/**
		 * Stage 1 sample size
		 */
		private int n1=50;
		/**
		 * Reads parameters from the JobConf object
		 */
		public void configure(JobConf conf) {
			n1 = conf.getInt("n1", 200);
			rbar = conf.getInt("maxBatch", 200);
			eta = Double.parseDouble(conf.get("eta"));
			answerMap = new HashMap<Integer, SOAnswer>();
			bestAnswerMap = new HashMap<Integer, SOAnswer>();
		}
		//
		//		private MultipleOutputs<IntWritable, Text> out;
		//
		//		public void setup(OutputCollector<IntWritable, Text> output, Reporter reporter) {
		//			out = new MultipleOutputs<IntWritable, Text>(context);
		//		}

		private Map<Integer, SOAnswer> answerMap = null;
		private Map<Integer, SOAnswer> bestAnswerMap = null;

		private void screen_with_best() {
			List<Integer> IDs = new ArrayList<Integer>(answerMap.keySet());
			List<Integer> IDs_best = new ArrayList<Integer>(bestAnswerMap.keySet());
			int nSys = IDs.size();
			int nSys_best = IDs_best.size();
			for (int i = 0; i < nSys; ++i) {
				SOAnswer ans_i = answerMap.get(IDs.get(i));
				if (ans_i.getElim() != 0) continue;
				double Xbar_i = ans_i.getFn();
				int ni = ans_i.getSize();
				double S2i = ans_i.getFnVar();
				int bi = ans_i.getBatchSize(); 
				for (int j = 0; j < nSys_best; ++j) {
					SOAnswer ans_j = bestAnswerMap.get(IDs_best.get(j));
					double Xbar_j = ans_j.getFn();
					int nj = ans_j.getSize();
					double S2j = ans_j.getFnVar();
					int bj = ans_j.getBatchSize();
					int maxi = n1 + rbar * bi;
					int maxj = n1 + rbar * bj;
					double tij = 1. / ( (S2i / maxi) + (S2j / maxj));
					double aij = Math.sqrt( (double) (tij * (n1-1)) ) * eta;
					double tau = 1.0/( (S2i/ni)+(S2j/nj) );
					double Y = tau*(Xbar_i-Xbar_j);

					if (Y < -aij) {
						// system i eliminated by system j
						ans_i.setElim(1);
					}
				}
			}

		}
		public void reduce(IntWritable key, Iterator<Text> values,
				OutputCollector<IntWritable, Text> output, Reporter reporter)
						throws IOException {
			long _tt = System.currentTimeMillis();
			tokenList.clear();
			//			int grp = key.get();
			answerMap.clear();
			bestAnswerMap.clear();
			while(values.hasNext()) {
				String line = values.next().toString();
				tokenList.clear();
				StringTokenizer tokenizer = new StringTokenizer(line);
				while (tokenizer.hasMoreTokens())
					tokenList.add(tokenizer.nextToken());

				int sysid = Integer.parseInt(tokenList.get(0));
				SOAnswer ans = null;

				//				if (line.endsWith("$Stage0") ) {
				//					// Stage 0 file: id S2 t $Stage0
				//					double S2 = Double.parseDouble(tokenList.get(1));
				//					// Add S2 and t to list
				//					ans = answerMap.get(sysid);
				//					if (ans == null) {
				//						ans = new SOAnswer();
				//						ans.setElim(1);
				//						answerMap.put(sysid, ans);
				//					}
				//					ans.setFnVar(S2);
				//				} else 
				if (line.endsWith("$Iter")) {
					// iter i-1 file: sysid Xbar n size seedString $Iter
					double Xbar = Double.parseDouble(tokenList.get(1));
					int n = Integer.parseInt(tokenList.get(2));
					int batchSize = Integer.parseInt(tokenList.get(3));
					double S2 = Double.parseDouble(tokenList.get(4));
					String seedString = tokenList.get(5);
					// Add Xbar, n and size to list
					ans = answerMap.get(sysid);
					if (ans == null) {
						ans = new SOAnswer();
						answerMap.put(sysid, ans);
					}
					ans.setFn(Xbar);
					ans.setSize(n);
					ans.setBatchSize(batchSize);
					ans.setFnVar(S2);
					ans.setSeedString(seedString);
					ans.setElim(0);
				} else if (line.endsWith("$Best")) {
					// best sys file: sysid Xbar n S2
					double bestXbar = Double.parseDouble(tokenList.get(1));
					int bestn = Integer.parseInt(tokenList.get(2));
					int bestBatchSize = Integer.parseInt(tokenList.get(3));
					double bestS2 = Double.parseDouble(tokenList.get(4));
					ans = bestAnswerMap.get(sysid);
					if (ans == null) {
						ans = new SOAnswer();
						bestAnswerMap.put(sysid, ans);
					}
					ans.setFn(bestXbar);
					ans.setSize(bestn);
					ans.setFnVar(bestS2);	
					ans.setBatchSize(bestBatchSize);
				}
			}
			long _t = System.currentTimeMillis();
			screen_with_best();
			_t = System.currentTimeMillis() - _t;
			reporter.incrCounter(Counters.SCREEN_TIME, _t);

			// for each surviving system
			List<Integer> IDs = new ArrayList<Integer>(answerMap.keySet());
			int nSys = IDs.size();
			for (int i = 0; i < nSys; ++i) {
				//				reporter.getCounter(MapRedRnS.Counters.SURVIVING_SYS).increment(1);
				int sysid = IDs.get(i);
				SOAnswer ans = answerMap.get(sysid);
				if (ans.getElim() == 0) {
					reporter.incrCounter(Counters.SURVIVING_SYS, 1);
					double Xbar = ans.getFn();
					int n = ans.getSize();
					int batchSize = ans.getBatchSize();
					double S2 = ans.getFnVar();
					String line = Double.toString(Xbar) + " " + Integer.toString(n) + " " +
							Integer.toString(batchSize) + " " + Double.toString(S2) + " " + 
							ans.getSeedString() + " " + " $Iter";
					output.collect(new IntWritable(sysid), new Text (line));
				}
			}// end for

			_tt = System.currentTimeMillis() - _tt;
			reporter.incrCounter(Counters.RED_TIME, _tt);
		}
	}

	/**
	 * Determine Rinott sample sizes, simply pass to reducer.
	 * <br>
	 * Input: [i Xbar n batchSize S^2 stream "$Sim"]
	 * <br>
	 * Output: i: {Xbar n S^2 stream "$Sim"}
	 * @author cn254
	 */
	public static class Stage3_PrepMapper extends MapReduceBase 
	implements Mapper<LongWritable, Text, IntWritable, Text> {
		/**
		 * Used to tokenize input
		 */
		private List<String> tokenList = new ArrayList<String>();

		public void map(LongWritable key, Text value,
				OutputCollector<IntWritable, Text> output, Reporter reporter)
						throws IOException {
			long _tt = System.currentTimeMillis();
			tokenList.clear();
			String line = value.toString();
			StringTokenizer tokenizer = new StringTokenizer(line);
			while (tokenizer.hasMoreTokens())
				tokenList.add(tokenizer.nextToken());

			int sysid = Integer.parseInt(tokenList.get(0));
			//			if(line.endsWith("$Stage0")) {
			//				double S2 = Double.parseDouble(tokenList.get(1));
			//				output.collect( new IntWritable( sysid ), new Text(Double.toString(S2)) );
			//			} else 
			if(line.endsWith("$Iter")){
				String Xbar = tokenList.get(1);
				String n = tokenList.get(2);
				String S2 = tokenList.get(4);
				String seed = tokenList.get(5);
				String out = Xbar + " " + n + " " + S2 + " " + seed + " $Iter";
				output.collect( new IntWritable( sysid ), new Text( out ) );
			}
			_tt = System.currentTimeMillis() - _tt;
			reporter.incrCounter(Counters.MAP_TIME, _tt);
		}
	}

	/**
	 * Determine Rinott sample sizes. Split additional Rinott sample to batches.
	 * <br>
	 * Input: i: {Xbar n S^2 stream "$Sim"}
	 * <br>
	 * Output 1: [i Xbar n "$S2"] for the Stage 2 sample
	 * <br>
	 * Output 2: [i stream batchSize "$S3"] for each additional batch
	 * @author cn254
	 */
	public static class Stage3_PrepReducer extends MapReduceBase 
	implements Reducer<IntWritable, Text, IntWritable, Text> {
		/**
		 * Used to tokenize input
		 */
		private List<String> tokenList = new ArrayList<String>();

		/**
		 * Indifference-zone parameter
		 */
		double delta;

		/**
		 * Rinott constant
		 */
		double h;
		
		/**
		 * Average batch size parameter beta
		 */
		int batchSize;

		/**
		 * Reads parameters from the JobConf object
		 */
		public void configure(JobConf conf) {
			delta = Double.parseDouble(conf.get("delta", "0.1"));
			h = Double.parseDouble(conf.get("h", "1."));
			batchSize = conf.getInt("batchSize", 100);
		}

		public void reduce(IntWritable key, Iterator<Text> values,
				OutputCollector<IntWritable, Text> output, Reporter reporter)
						throws IOException {
			long _tt = System.currentTimeMillis();
			tokenList.clear();
			double Xbar = 0, S2 = 0;
			String seedStr = null;
			int n = 0;
			while(values.hasNext()) {
				String line = values.next().toString();
				tokenList.clear();
				StringTokenizer tokenizer = new StringTokenizer(line);
				while (tokenizer.hasMoreTokens())
					tokenList.add(tokenizer.nextToken());
				//				if (line.endsWith("$Iter")) {
				Xbar = Double.parseDouble(tokenList.get(0));
				n = Integer.parseInt(tokenList.get(1));
				S2 = Double.parseDouble(tokenList.get(2));
				seedStr = tokenList.get(3);
				//				} else
				//					S2 = Double.parseDouble(line);
			}

			//return if the system has been eliminated
			if ( seedStr == null ) return;

			long [] seed = RngStream.StrToSeed(seedStr);
			RngStream rStream = new RngStream();
			rStream.setSeed(seed);

			//calculate Rinott size
			int RinottSize = (int)Math.ceil(h * h * S2 / delta / delta);
			int additionalSize = RinottSize-n;
			additionalSize = (additionalSize>0)?additionalSize:0;
			// output 1: additional batches for each system
			while(additionalSize > 0) {
				double []state = rStream.getState(); 
				for(int i = 0; i < 6; ++i) seed[i] = (long) state[i];
				seedStr = RngStream.SeedToStr(seed);
				int nextBatchSize = additionalSize > batchSize ? batchSize:additionalSize;
				additionalSize -= nextBatchSize;
				String out1 = seedStr + " " + Integer.toString(nextBatchSize) + " $Stage2";
				output.collect(key, new Text(out1) );
				rStream.resetNextSubstream();
			}
			//	output 2: carry Stage 1 sample over
			String out = Double.toString(Xbar) + " " + Integer.toString(n) + " $Stage2Existing";
			output.collect(key, new Text(out) );
			if (RinottSize > n) {
				reporter.incrCounter(Counters.NUM_RINOTT, 1);	
				reporter.incrCounter(Counters.RINOTT_SIZE, RinottSize - n);
			}
			_tt = System.currentTimeMillis() - _tt;
			reporter.incrCounter(Counters.RED_TIME, _tt);
		}
	}	

	/**
	 * Simulate additional Rinott batches.
	 * <br>
	 * Input 1: [i Xbar n "$S2"], Stage 2 sample to be passed on to reducer
	 * <br>
	 * Output 1: 1: {i Xbar n "$S2"}
	 * <br>
	 * Input 2: [i stream batchSize "$S3"] additional batch to be simulated
	 * <br>
	 * Output 2: 1: {i Xbar batchsize "$S3"}
	 * @author cn254
	 */
	public static class Stage3Mapper extends MapReduceBase implements 
	Mapper<LongWritable, Text, IntWritable, Text> {

		/**
		 * Parameter RB for the TpMax test problem
		 */
		private String param = null;
		/**
		 * Parameter for randomizing completion time
		 */
		private String cov = null;

		/**
		 * Reads parameters from the JobConf object
		 */
		public void configure(JobConf conf) {
			param = conf.get("param");
			cov = conf.get("cov");
		}
		/**
		 * Used to tokenize input
		 */
		private List<String> tokenList = new ArrayList<String>();
		private IntWritable one = new IntWritable(1);
		public void map(LongWritable key, Text value,
				OutputCollector<IntWritable, Text> output, Reporter reporter)
						throws IOException {
			long _tt = System.currentTimeMillis();
			tokenList.clear();
			String line = value.toString();
			StringTokenizer tokenizer = new StringTokenizer(line);
			while (tokenizer.hasMoreTokens())
				tokenList.add(tokenizer.nextToken());
			if (line.endsWith("$Stage2") ) {
				// value = [sysid , seedString(substream i) RinottSize(batch i) "$Stage2"], 
				// simulate a batch of Rinott sample
				// emits(key= 1, value = sysid, Xbar(batch i) RinottSize(batch i))
				int sysid = Integer.parseInt(tokenList.get(0));
				long [] seed = RngStream.StrToSeed(tokenList.get(1));
				int size = Integer.parseInt(tokenList.get(2));
				assert(size > 0);
				String out = tokenList.get(0) + " ";

				RngStream rStream = new RngStream();
				rStream.setSeed(seed);
				SOProb prob = new TpMax(param,cov);
				long _t = System.nanoTime();
				prob.runSystem(sysid, size, rStream);
				_t = System.nanoTime() - _t;
				reporter.incrCounter(Counters.P2_TIME, _t);
				reporter.incrCounter(Counters.REP_COUNT, size);
				SOAnswer ans = prob.getAns();
				out = out + Double.toString(ans.getFn()) + " " + size;
				output.collect(one, new Text(out));
			} else {
				//values = [sysid, Xbars RinottSize]
				assert (line.endsWith("$Stage2Existing"));
				String out = tokenList.get(0) + " " + tokenList.get(1) + " " + tokenList.get(2);
				output.collect(one, new Text(out));
			}
			_tt = System.currentTimeMillis() - _tt;
			reporter.incrCounter(Counters.MAP_TIME, _tt);
		}
	}

	/**
	 * Merge batches and find the best system.
	 * <br>
	 * Input 1: 1: {i Xbar n "$S2"}
	 * <br>
	 * Input 2: 1: {i Xbar n "$S3"} for every batch
	 * <br>
	 * Output 1: [i Xbar n] for every system entering Stage 3
	 * <br>
	 * Output 2: The ID of the best system
	 * @author cn254
	 */
	public static class Stage3Reducer extends MapReduceBase 
	implements Reducer<IntWritable, Text, NullWritable, Text> {
		/**
		 * Used to tokenize input
		 */
		private List<String> tokenList = new ArrayList<String>();
		private MultipleOutputs multipleOutputs; 

		/**
		 * Reads parameters from the JobConf object
		 */
		@Override
		public void configure(JobConf conf) {
			multipleOutputs = new MultipleOutputs(conf);
		}
		//		private MultipleOutputs<Text, Text> out;

		//		@Override
		//		public void setup(OutputCollector<IntWritable, Text> output, Reporter reporter) {
		//			out = new MultipleOutputs<Text, Text>(context);
		//		}

		public void reduce(IntWritable key, Iterator<Text> values,
				OutputCollector<NullWritable, Text> output, Reporter reporter)
						throws IOException {
			long _tt = System.currentTimeMillis();
			String line = null;
			int bestid = -1;
			double bestXbar = -1;

			@SuppressWarnings("unchecked")
			OutputCollector<IntWritable, Text> collector = 
			(OutputCollector<IntWritable, Text>)multipleOutputs.getCollector("Stage2Stats",
					key.toString().replace("-", ""), reporter);
			@SuppressWarnings("unchecked")
			OutputCollector<NullWritable, Text> collector2 = 
			(OutputCollector<NullWritable, Text>)multipleOutputs.getCollector("Best",
					key.toString().replace("-", ""), reporter);
			HashMap<Integer, SOAnswer> Sys2AnsMap = new HashMap<Integer, SOAnswer>();

			//each line is a batch belongs to system with sysid
			while(values.hasNext()) {
				line = values.next().toString();
				tokenList.clear();
				StringTokenizer tokenizer = new StringTokenizer(line);
				while (tokenizer.hasMoreTokens())
					tokenList.add(tokenizer.nextToken());
				int sysid = Integer.parseInt(tokenList.get(0));
				double Xbar = Double.parseDouble(tokenList.get(1));
				int size =  Integer.parseInt(tokenList.get(2));	
				if (Sys2AnsMap.containsKey(sysid)) {
					SOAnswer ans = Sys2AnsMap.get(sysid);
					ans.addSample(Xbar, size);
				} else {
					SOAnswer ans = new SOAnswer();
					ans.setFn(Xbar);
					ans.setSize(size);
					Sys2AnsMap.put(sysid, ans);
				}
				//				collector.collect(new IntWritable(sysid), new Text("" + Xbar + " " + size), 
				//						"Stage2Stats/part");					
			}
			//finds the system with the highest system mean
			for (Map.Entry<Integer, SOAnswer> entry : Sys2AnsMap.entrySet()) {
				int sysid = entry.getKey();
				double Xbar = entry.getValue().getFn();
				int size = entry.getValue().getSize();

				if (Xbar > bestXbar) {
					bestid = sysid;
					bestXbar = Xbar;
				}		
				collector.collect(new IntWritable(sysid), new Text("" + Xbar + " " + size));
			}
			collector2.collect(NullWritable.get(), 
					new Text( "Best System id: " + bestid + ", with mean: " + bestXbar));

			reporter.incrCounter(Counters.BEST_ID, bestid);
			//			out.write(new IntWritable(1), 
			//					new Text( "Best: System id " + bestid + " with mean: " + bestXbar), 
			//					"Stage2Best/part");
			_tt = System.currentTimeMillis() - _tt;
			reporter.incrCounter(Counters.RED_TIME, _tt);
		} 

		@Override
		public void close() throws IOException {
			multipleOutputs.close();
		}

		//		@Override
		//		public void cleanup(OutputCollector<IntWritable, Text> output, Reporter reporter) throws IOException {
		//			out.close();
		//		}
	}

	/**
	 * Drives the parallel MapReduce procedure
	 * @param args Inputs are as follows: [inputPath] [outputPath] [Stage 1 sample size n1] [average batch size beta]
	 * [number of cores] [seed] [maximum sample size (= rbar * beta)] [delta] 
	 * [parameter RB] [random completion time parameter] 
	 * @throws Exception Not used.
	 */
	//	@Override
	//	public int run (String[] args) throws Exception {
	//		System.setProperty("HADOOP_USER_NAME", "ubuntu");
	public static void main(String[] args) throws Exception {
		boolean runS0 = true;		
		boolean runS1 = true;
		boolean runS2 = true;
		boolean useSpeculative = true;
		int iteration = -1;
		boolean prof_switch = false;
		int prof_depth = 10;
		String prof_args = "-agentlib:hprof=cpu=times,heap=sites,depth=" + Integer.toString(prof_depth)+ "," + 
				"force=n,thread=y,verbose=n,interval=5,file=%s";

		System.out.println("args[0] = " + args[0]); 
		String inputPath = args[1];
		String outputPath = args[2];
		System.out.println("outputPath = " + outputPath); 
		int i = 3;
		int n1 = Integer.parseInt(args[i++]);  //50
		int batchSize = Integer.parseInt(args[i++]); // 100
		int nCores = Integer.parseInt(args[i++]); // 10
		int seed = Integer.parseInt(args[i++]); // 14853
		int s1Max = Integer.parseInt(args[i++]); // 1000
		double delta = Double.parseDouble(args[i++]); // 0.1
		String param = args[i++]; // 20/50/128
		String cov = args[i++]; // 0.05/0.2
		long nSys = TpMax.getNumSystems(param);
		System.out.println("Total Number of Systems = " + nSys); 
		String eta = Double.toString(EtaFunc.find_eta(n1, 0.025, nSys));
		System.out.println("eta = " + eta);
		int maxBatch = s1Max / batchSize;

		int numMappers, numReducers;
		int nGroups = 0;
		int nGroups2=0;
		if (nCores > 1000) {
			numMappers  = nCores - 1;
			numReducers = numMappers;
			nGroups = nCores/2 - 1;
			nGroups2 = nCores / 4;
		}	else {
			numMappers = nCores * 4;
			numReducers = 32;
			nGroups = numReducers;
			numMappers = 4*13 - 4;
			nGroups = nGroups2 = nCores;
		}
		if (args.length > i) {
			numReducers = Integer.parseInt(args[i]);
		}

		long start = System.currentTimeMillis();
		FileSystem fs = null;
		Path outPath = null;
		RunningJob rj = null;
		JobConf conf = null;
		long surviving = 197;
		long simcount_0, simcount_1, simcount_2, winner;
		simcount_0 = simcount_1 = simcount_2 = winner = 0;
		double time_0,time_1,time_2;
		time_0 = time_1 = time_2 = 0.0;
		double screen_t = 0.0;
		double total_map_time, total_red_time;
		total_map_time = total_red_time = 0.0;

		if (runS0) {

			conf = new JobConf(prands.class);
			conf.setJarByClass(prands.class);
			conf.setInt("n1", n1);
			conf.setInt("seed", seed);
			conf.set("param", param);
			conf.set("cov", cov);
			//		Configuration conf = new Configuration();
			fs = FileSystem.get(conf);
			conf.setJobName("Stage0-Init");
			conf.setMapOutputKeyClass(IntWritable.class);
			conf.setMapOutputValueClass(Text.class);		
			conf.setOutputKeyClass(IntWritable.class);
			conf.setOutputValueClass(Text.class);
			conf.setMapperClass(Stage1InitMapper.class);
			conf.setReducerClass(Stage1InitReducer.class);
			conf.setInputFormat(TextInputFormat.class);
			conf.setOutputFormat(TextOutputFormat.class);
			if(!useSpeculative) conf.setSpeculativeExecution(false);
			FileInputFormat.setInputPaths(conf, new Path(inputPath));
			outPath = new Path(outputPath + "/Stage0_Init");
			if (fs.exists(outPath))			fs.delete(outPath, true);
			FileOutputFormat.setOutputPath(conf, outPath);
			conf.setNumReduceTasks(numReducers);	
			conf.setNumMapTasks(numMappers);
			if (prof_switch) {
				conf.setProfileEnabled(true);
				conf.setProfileParams(prof_args);
				conf.setProfileTaskRange(true, "0-500");
			}
			rj = JobClient.runJob(conf);	

			time_0 = rj.getCounters().getCounter(Counters.P0_TIME) / 1000000000.0;
			total_map_time += rj.getCounters().getCounter(Counters.MAP_TIME) / 1000.0;
			total_red_time += rj.getCounters().getCounter(Counters.RED_TIME) / 1000.0;
			long TotalSys = rj.getCounters().getCounter(Counters.NUM_SYS);
			double avgS = (double) 
					(rj.getCounters().getCounter(Counters.TOTAL_S2T)) 
					/ 10000./TotalSys;

			//		job0 = null;
			conf = null;
			outPath = null;
			//		fs = null;		
			System.out.println("End of Stage 0, Total Sys = " + TotalSys + ", avgS = " + avgS);

			conf = new JobConf(prands.class);
			conf.setJarByClass(prands.class);
			conf.setInt("batchSize", batchSize);
			conf.setLong("TotalSys", TotalSys);
			conf.setInt("n1", n1);
			conf.setInt("maxBatch", maxBatch);
			conf.set("eta", eta);
			conf.set("avgS", Double.toString(avgS));
			conf.setInt("nGroups", nGroups2);

			fs = FileSystem.get(conf);
			conf.setJobName("Stage0-Collect");
			conf.setMapOutputKeyClass(IntWritable.class);
			conf.setMapOutputValueClass(Text.class);		
			//			conf.setOutputKeyClass(IntWritable.class);
			//			conf.setOutputValueClass(Text.class);
			conf.setMapperClass(Stage1Mapper.class);
			conf.setReducerClass(GroupScreenReducer.class);
			//Use identity reducer
			//			conf.setReducerClass(IdentityReducer.class);
			//			conf.setReducerClass(Stage0Reducer.class);
			conf.setInputFormat(TextInputFormat.class);
			conf.setOutputFormat(TextOutputFormat.class);
			if(!useSpeculative) conf.setSpeculativeExecution(false);
			FileInputFormat.setInputPaths(conf, new Path(outputPath + "/Stage0_Init"));
			outPath = new Path(outputPath + "/Stage0_Screen");
			if (fs.exists(outPath))			fs.delete(outPath, true);
			FileOutputFormat.setOutputPath(conf, outPath);
			conf.setNumReduceTasks(numReducers);	
			conf.setNumMapTasks(numMappers);
			if (prof_switch) {
				conf.setProfileEnabled(true);
				conf.setProfileParams(prof_args);
				conf.setProfileTaskRange(true, "0-500");
			}

			rj = JobClient.runJob(conf);
			total_map_time += rj.getCounters().getCounter(Counters.MAP_TIME) / 1000.0;
			total_red_time += rj.getCounters().getCounter(Counters.RED_TIME) / 1000.0;
			screen_t += rj.getCounters().getCounter(Counters.SCREEN_TIME) / 1000.0;
			simcount_0 = nSys * (long) n1;

			////////////////////////// Screen with best Job //////////////////////////
			//
			iteration = 0;
			conf = new JobConf(prands.class);
			conf.setJarByClass(prands.class);
			conf.setInt("nGroups", nGroups2);
			conf.setInt("maxBatch", maxBatch);
			conf.set("eta", eta);
			conf.setInt("n1", n1);
			conf.setJobName("Stage0 iteration " + iteration + " (best)" );
			conf.setMapOutputKeyClass(IntWritable.class);
			conf.setMapOutputValueClass(Text.class);		
			conf.setOutputKeyClass(IntWritable.class);
			conf.setOutputValueClass(Text.class);
			conf.setMapperClass(ScreenBestMapper.class);
			conf.setReducerClass(ScreenBestReducer.class);
			conf.setInputFormat(TextInputFormat.class);
			conf.setOutputFormat(TextOutputFormat.class);
			if(!useSpeculative) conf.setSpeculativeExecution(false);
			FileInputFormat.setInputPaths(conf, new Path(outputPath + "/Stage0_Screen"));
			outPath = new Path(outputPath + "/i" + iteration);
			if (fs.exists(outPath))			fs.delete(outPath, true);
			FileOutputFormat.setOutputPath(conf, outPath);
			conf.setNumReduceTasks(numReducers);	
			conf.setNumMapTasks(numMappers);
			if (prof_switch) {
				conf.setProfileEnabled(true);
				conf.setProfileParams(prof_args);
				conf.setProfileTaskRange(true, "0-500");
			}

			rj = JobClient.runJob(conf);
			total_map_time += rj.getCounters().getCounter(Counters.MAP_TIME) / 1000.0;
			total_red_time += rj.getCounters().getCounter(Counters.RED_TIME) / 1000.0;
			screen_t += rj.getCounters().getCounter(Counters.SCREEN_TIME) / 1000.0;
			surviving  = rj.getCounters().getCounter(Counters.SURVIVING_SYS);
		}

		if (runS1) {
			iteration = 1;
			while (iteration < 2 * maxBatch && surviving > 1) {
				////////////////////////// Stage 1 Job //////////////////////////
				//			Configuration conf2 = new Configuration();
				//			fs = FileSystem.get(conf2);
				conf = new JobConf(prands.class);
				conf.setJarByClass(prands.class);
				conf.setInt("nGroups", nGroups);
				conf.setInt("iteration", iteration);
				conf.setInt("maxBatch", maxBatch);
				conf.setInt("n1", n1);
				conf.set("param", param);
				conf.set("cov", cov);
				conf.set("eta", eta);

				conf.setJobName("Stage1 iteration " + iteration);
				conf.setMapOutputKeyClass(IntWritable.class);
				conf.setMapOutputValueClass(Text.class);		
				conf.setOutputKeyClass(IntWritable.class);
				conf.setOutputValueClass(Text.class);
				conf.setMapperClass(Stage2Mapper.class);
				conf.setReducerClass(GroupScreenReducer.class);
				conf.setInputFormat(TextInputFormat.class);
				conf.setOutputFormat(TextOutputFormat.class);
				if(!useSpeculative) conf.setSpeculativeExecution(false);
				FileInputFormat.setInputPaths(conf, new Path(outputPath + "/i"
						+ (iteration - 1)));
				//				FileInputFormat.addInputPath(conf, new Path(outputPath + "/Stage0"));
				outPath = new Path(outputPath + "/i" + iteration);
				if (fs.exists(outPath))			fs.delete(outPath, true);
				FileOutputFormat.setOutputPath(conf, outPath);
				conf.setNumReduceTasks(numReducers);	
				conf.setNumMapTasks(numMappers);
				if (prof_switch) {
					conf.setProfileEnabled(true);
					conf.setProfileParams(prof_args);
					conf.setProfileTaskRange(true, "0-500");
				}

				rj = JobClient.runJob(conf);
				total_map_time += rj.getCounters().getCounter(Counters.MAP_TIME) / 1000.0;
				total_red_time += rj.getCounters().getCounter(Counters.RED_TIME) / 1000.0;
				time_1 += rj.getCounters().getCounter(Counters.P1_TIME) / 1000000000.0;
				screen_t += rj.getCounters().getCounter(Counters.SCREEN_TIME) / 1000.0;
				long rep_count = rj.getCounters().getCounter(Counters.REP_COUNT);
				simcount_1 += rep_count;
				iteration++;

				////////////////////////// Screen with best Job //////////////////////////
				//

				conf = new JobConf(prands.class);
				conf.setJarByClass(prands.class);
				conf.setInt("nGroups", nGroups);
				conf.setInt("maxBatch", maxBatch);
				conf.set("eta", eta);
				conf.setInt("n1", n1);
				conf.setJobName("Stage1 iteration " + iteration + " (best)" );
				conf.setMapOutputKeyClass(IntWritable.class);
				conf.setMapOutputValueClass(Text.class);		
				conf.setOutputKeyClass(IntWritable.class);
				conf.setOutputValueClass(Text.class);
				conf.setMapperClass(ScreenBestMapper.class);
				conf.setReducerClass(ScreenBestReducer.class);
				conf.setInputFormat(TextInputFormat.class);
				conf.setOutputFormat(TextOutputFormat.class);
				if(!useSpeculative) conf.setSpeculativeExecution(false);
				FileInputFormat.setInputPaths(conf, new Path(outputPath + "/i" 
						+ (iteration - 1) ) );
				//				FileInputFormat.addInputPath(conf, new Path(outputPath + "/Stage0"));
				outPath = new Path(outputPath + "/i" + iteration);
				if (fs.exists(outPath))			fs.delete(outPath, true);
				FileOutputFormat.setOutputPath(conf, outPath);
				conf.setNumReduceTasks(numReducers);	
				conf.setNumMapTasks(numMappers);
				if (prof_switch) {
					conf.setProfileEnabled(true);
					conf.setProfileParams(prof_args);
					conf.setProfileTaskRange(true, "0-500");
				}

				rj = JobClient.runJob(conf);
				total_map_time += rj.getCounters().getCounter(Counters.MAP_TIME) / 1000.0;
				total_red_time += rj.getCounters().getCounter(Counters.RED_TIME) / 1000.0;
				screen_t += rj.getCounters().getCounter(Counters.SCREEN_TIME) / 1000.0;
				surviving  = rj.getCounters().getCounter(Counters.SURVIVING_SYS);
				iteration++;
			} 	
		}
		if (surviving > 1 && runS2) {
			iteration = 2 * maxBatch + 1;
			double rinott_h = Rinott.rinott(surviving, 0.975, n1-1);
			conf = new JobConf(prands.class);
			conf.setJarByClass(prands.class);
			fs = FileSystem.get(conf);
			//			conf = new Configuration();
			conf.set("h", Double.toString(rinott_h));
			conf.setLong("surviving", surviving);		
			conf.setInt("batchSize", batchSize);
			conf.set("delta", Double.toString(delta));
			conf.setJobName("Stage2-Prep");
			conf.setMapOutputKeyClass(IntWritable.class);
			conf.setMapOutputValueClass(Text.class);		
			conf.setOutputKeyClass(IntWritable.class);
			conf.setOutputValueClass(Text.class);
			conf.setMapperClass(Stage3_PrepMapper.class);
			conf.setReducerClass(Stage3_PrepReducer.class);
			conf.setInputFormat(TextInputFormat.class);
			conf.setOutputFormat(TextOutputFormat.class);
			if(!useSpeculative) conf.setSpeculativeExecution(false);
			FileInputFormat.setInputPaths(conf, new Path(outputPath + "/i"
					+ (iteration - 1)));
			//			FileInputFormat.addInputPath(conf, new Path(outputPath + "/Stage0"));
			outPath = new Path(outputPath + "/Stage2");
			if (fs.exists(outPath))			fs.delete(outPath, true);
			FileOutputFormat.setOutputPath(conf, outPath);
			conf.setNumReduceTasks(numReducers);	
			conf.setNumMapTasks(numMappers);
			if (prof_switch) {
				conf.setProfileEnabled(true);
				conf.setProfileParams(prof_args);
				conf.setProfileTaskRange(true, "0-500");
			}
			rj = JobClient.runJob(conf);
			total_map_time += rj.getCounters().getCounter(Counters.MAP_TIME) / 1000.0;
			total_red_time += rj.getCounters().getCounter(Counters.RED_TIME) / 1000.0;

			rj = null; 
			conf = null;
			conf = new JobConf(prands.class);
			conf.setJarByClass(prands.class);
			conf.set("param", param);
			conf.set("cov", cov);
			//			conf.setLong("mapred.task.timeout", 100*60*1000); // 100 minutes
			conf.setJobName("Stage2-Full");
			conf.setMapOutputKeyClass(IntWritable.class);
			conf.setMapOutputValueClass(Text.class);	
			conf.setOutputKeyClass(NullWritable.class);
			conf.setOutputValueClass(Text.class);
			conf.setMapperClass(Stage3Mapper.class);
			conf.setReducerClass(Stage3Reducer.class);
			conf.setInputFormat(TextInputFormat.class);
			conf.setOutputFormat(NullOutputFormat.class);
			if(!useSpeculative) conf.setSpeculativeExecution(false);
			FileInputFormat.setInputPaths(conf, new Path(outputPath + "/Stage2"));
			outPath = new Path(outputPath + "/Final");
			if (fs.exists(outPath))			fs.delete(outPath, true);
			FileOutputFormat.setOutputPath(conf, outPath);
			conf.setNumReduceTasks(numReducers);	
			conf.setNumMapTasks(numMappers);
			MultipleOutputs.addMultiNamedOutput(conf, "Stage2Stats", TextOutputFormat.class,
					IntWritable.class, Text.class);
			MultipleOutputs.addMultiNamedOutput(conf, "Best", TextOutputFormat.class,
					NullWritable.class, Text.class);
			if (prof_switch) {
				conf.setProfileEnabled(true);
				conf.setProfileParams(prof_args);
				conf.setProfileTaskRange(true, "0-500");
			}
			rj = JobClient.runJob(conf);
			total_map_time += rj.getCounters().getCounter(Counters.MAP_TIME) / 1000.0;
			total_red_time += rj.getCounters().getCounter(Counters.RED_TIME) / 1000.0;
			time_2 = rj.getCounters().getCounter(Counters.P2_TIME) / 1000000000.0;
			simcount_2 = rj.getCounters().getCounter(Counters.REP_COUNT);		
			winner = rj.getCounters().getCounter(Counters.BEST_ID);			
			// Submit the job, then poll for progress until the job is complete
			//			job.setJarByClass(MapRedRnS.class);
			//			job.waitForCompletion(true);
		}
		long simTotal = simcount_0 + simcount_1 + simcount_2;
		double timeTotal = time_0 + time_1 + time_2;
		long end = System.currentTimeMillis();
		double running_time = (end - start) / 1000.0;
		System.out.println("running time " + running_time + "s");
		System.out.println("algo,n0,n1,bsize,bmax,delta,seed,screenVer,core,time," +
				"RB,cov,systems,stage2sys,simcount_0,simcount_1,simcount_2,simcount_3," +
				"totalsim,winner," + 
				"simtime_0,simtime_1,simtime_2,simtime_3,simtime_total," +
				"simtime_util,screentime," +
				"screentime_util,total_map,total_red");
		String outStr = "HADOOP,0," + Integer.toString(n1) + 
				"," + Integer.toString(batchSize) + "," + Integer.toString(maxBatch) + 
				"," + Double.toString(delta)  +
				"," + Integer.toString(seed) + 
				"," + "9" + "," + Integer.toString(nCores)  + "," + running_time + 
				"," + param + "," + cov + "," + Long.toString(nSys)  + 
				"," + Long.toString(surviving)  +
				",0," + Long.toString(simcount_0) + 
				"," + Long.toString(simcount_1)  + "," + Long.toString(simcount_2)  +
				"," + Long.toString(simTotal)  +
				"," + winner + 
				",0," + time_0 + "," + time_1  + "," + time_2  +
				"," + timeTotal + "," + Double.toString(timeTotal / running_time / nCores) + 
				"," + screen_t + "," + Double.toString(screen_t / running_time / nCores) + 
				"," + total_map_time + 
				"," + total_red_time;
		System.out.println(outStr);
		//		return 0;
	}
}
