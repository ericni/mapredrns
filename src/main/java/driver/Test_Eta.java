package driver;

import static org.junit.Assert.*;

import org.junit.*;

import problems.TpMax;


/**
 * Simple JUnit test for the EtaFunc.find_eta method. 
 * Answers are compared with results from running FindRoot in Mathematica.
 * @author cn254
 *
 */

public class Test_Eta {

	@Test
	public void test() {

		double    ans[] = { 9.7846,	0.8730,	1.7233,	2.0856,	1.10322	};
		int 	   n1[] = { 10,		50,		20,		20,		50		};
		double alpha1[] = { .0025,	.025,	.025,	.025,	.025	};
		long   	   RB[] = { 128,	20,		20,		50,		128		};
		for (int i = 0; i < ans.length; ++i) {			
			double result = EtaFunc.find_eta(n1[i], alpha1[i], 
					TpMax.getNumSystems(Long.toString(RB[i])));
			assertEquals( result, ans[i], 0.01);
		}
	}
}
