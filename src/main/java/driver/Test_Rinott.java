package driver;

import static org.junit.Assert.*;
import org.junit.*;


/**
 * Simple JUnit test for the Rinott.rinott method.
 * Answers are compared with results from running the original Fortran program.
 * @author cn254
 *
 */
public class Test_Rinott {

	@Test
	public void testRinott() {
		// calculated by Fortran code in
		// Design and Analysis of Experiments for Statistical Selection, 
		// Screening, and Multiple Comparisons
		// Robert E. Bechhofer, Thomas J. Santner, David M. Goldsman
		// ISBN: 978-0-471-57427-9
		// Wiley, 1995
		// Fortran code also available on 
		// http://www.stat.osu.edu/~tjs/REB-TJS-DMG/describe.html
 		double ans[] = { 4.045,	6.057,	6.893,	5.488,	6.878,	8.276,	8.352  	};
		double p[]   = { .975,	.975,	.975,	.9,		.95,	0.975,	0.975	};
		long   n[]   = { 10,	1000,	10000,	1000,	20000,	800000,	1000000	};
		for (int i = 0; i < n.length; ++i) {
			double result = Rinott.rinott(n[i], p[i], 50);
			assertTrue(result > ans[i] - 0.01 && result < ans[i] + 0.3);
		}
	}
}